FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/*.jar

COPY ${JAR_FILE} shop.jar

EXPOSE 8080
ENTRYPOINT ["java","-jar","/shop.jar"]