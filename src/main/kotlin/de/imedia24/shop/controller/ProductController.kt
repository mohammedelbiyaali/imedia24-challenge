package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(
        @RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for products $skus")

        val products = productService.findProductsBySkus(skus)
        return if(products.isEmpty()) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PostMapping("/product", produces = ["application/json;charset=utf-8"])
    fun addProduct(
        @RequestBody product: ProductResponse
    ): ResponseEntity<HttpStatus> {
        logger.info("Request to save product ${product.sku} in DB")

        return try {
            productService.addProduct(product)
            ResponseEntity<HttpStatus>(HttpStatus.CREATED)
        } catch (e: Exception) {
            logger.error("Could not save ${product.sku} due to $e")
            ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PutMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @PathVariable("sku") sku: String,
        @RequestBody product: ProductResponse
    ): ResponseEntity<HttpStatus> {
        logger.info("Request to update product ${product.sku} in DB")

        return if (productService.updateProduct(product)) {
            ResponseEntity<HttpStatus>(HttpStatus.OK)
        } else {
            ResponseEntity<HttpStatus>(HttpStatus.NOT_FOUND)
        }
    }
}
