package de.imedia24.shop.configuration

import org.springframework.context.annotation.Configuration
import springfox.documentation.swagger2.annotations.EnableSwagger2
import org.springframework.context.annotation.Bean
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket

@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    @Bean
    fun swaggerConf(): Docket{
	    return Docket(DocumentationType.SWAGGER_2)
		    .select()
		    .paths(PathSelectors.any())
			.apis(RequestHandlerSelectors.basePackage("de.imedia24.shop"))
		    .build()
		    .apiInfo(getApiInfo())
    }

    private fun getApiInfo(): ApiInfo {
	    return ApiInfoBuilder()
		    .title("Imedia shop")
		    .description("Example Api Documentation")
		    .version("0.0.1")
		    .build()
    }
}
