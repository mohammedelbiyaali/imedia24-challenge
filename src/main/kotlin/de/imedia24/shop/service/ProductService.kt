package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    private val logger = LoggerFactory.getLogger(ProductService::class.java)!!

    fun findProductBySku(sku: String): ProductResponse? {
        logger.info("DB Search for product $sku")

        return productRepository.findProductEntityBySku(sku)?.toProductResponse()
    }

    fun findProductsBySkus(skus: List<String>): List<ProductResponse> {
        logger.info("DB Search for products $skus")

        val products: MutableList<ProductResponse> = mutableListOf()
        productRepository.findProductEntitiesBySkuIn(skus).forEach {
            products.add(it.toProductResponse())
        }

        return  products.toList()
    }

    fun addProduct(product: ProductResponse) {
        logger.info("Save product ${product.sku} in DB")

        val productEntity = ProductEntity(
            sku = product.sku,
            name = product.name,
            description = product.description,
            price = product.price,
            stockInfo = product.stockInfo,
            createdAt = ZonedDateTime.now(),
            updatedAt = ZonedDateTime.now()
        )
        productRepository.save(productEntity)
    }

    fun updateProduct(product: ProductResponse): Boolean {
        val productEntity = productRepository.findProductEntityBySku(product.sku)

        return if (productEntity == null) {
            logger.warn("Product ${product.sku} does not exist in DB")
            false
        } else {
            logger.info("Update product ${product.sku}")

            productEntity.name = product.name
            productEntity.description = product.description
            productEntity.price = product.price
            productEntity.stockInfo = product.stockInfo
            productEntity.updatedAt = ZonedDateTime.now()

            productRepository.save(productEntity)
            true
        }
    }
}
