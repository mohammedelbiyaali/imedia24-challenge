In the project root run the following commands (Same level as the Dockefile):

gradlew build
docker build --build-arg JAR_FILE=build/libs/\*.jar -t imedia-shop .
docker run -p 8080:8080 imedia-shop
